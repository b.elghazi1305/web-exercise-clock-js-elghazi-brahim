
var s=document.querySelector(".clock_second");
var m=document.querySelector(".clock_minutes");
var h=document.querySelector(".clock_hours");

var h_2=document.getElementById("hour_h");  
var m_2=document.getElementById("hour_m"); 
var s_2=document.getElementById("hour_s");  
s.style.transformOrigin="center center";
m.style.transformOrigin="center center";
h.style.transformOrigin="center center";
function Get_degS(){
    let now = new Date();
    current = now.getSeconds(); 
    if(current<10){ current="0"+current;}
    s_2.innerHTML =current;
    let deg=(current*360)/60;
    return deg;
}
function Get_degM(){
    let now = new Date();
    current = now.getMinutes(); 
    if(current<10){ current="0"+current;}
    m_2.innerHTML =current;
    let deg=(current*360)/60;
    return deg;
}
function Get_degH(){
    let now = new Date();
    current = now.getHours(); 
    if(current<10){ current="0"+current;}
    h_2.innerHTML =current;
    let deg=(current*360)/24;
    return deg;
}

function rolate_s(){
    let a=Get_degS();
    s.style.transform ="rotate("+a+"deg)";
}
function rolate_m(){
    let b=Get_degM();
    m.style.transform ="rotate("+b+"deg)";
}
function rolate_h(){
    let c=Get_degH();
    h.style.transform ="rotate("+c+"deg)";
}
rolate_s();
rolate_m();
rolate_h();
setInterval(rolate_s, 1000);
setInterval(rolate_m, 60000);
setInterval(rolate_h, 360000);



